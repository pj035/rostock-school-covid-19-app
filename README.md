# Covid 19 App

A simple school project to learn development of an Ionic mobile app.

We are using the covid 19 API provided in https://gitlab.com/pj035/rostock-school-covid-19-geo-api to fetch data in Germany according to the user's device position.

A step by step guide will be added soon to this readme.